import "dotenv/config";
import express from "express";
import mongoose from "mongoose";
import db from "./db";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import session from "express-session";
import connectmongo from "connect-mongo";
const MongoStore = connectmongo(session);
import passportsocketio from "passport.socketio";
import passport from "passport";
import path from "path";
import chalk from "chalk";
import socketio from "socket.io";
import readline from "readline";
import pick from "object.pick";
import { register } from "./routes";

// connect to db
db.init();

// Setup Express server

console.log("[Express] Starting server");

const app = express();
const port = process.env.PORT || 8080;

// full path to this file as an array
let fullpath = path.dirname(__filename).split(path.sep);

// go to the project root
fullpath = fullpath.slice(0, fullpath.length - 2);

// path to client build dir
const clientpath = path.join(fullpath.join(path.sep), "client", "build");

// Serve the static files from the React app
app.use(express.static(clientpath));

// cookie parser middleware
app.use(cookieParser());

// body parser to read POSTs
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// session middleware
const sessionOptions = {
	key: "connect.sid",
	// just some random text
	secret: `rawrx3nuzzleshowareyoupouncesonyouyouresowarmo3onoticesyouhaveabulgeosomeoneshappynuzzlesyourneckyweckymurrheheherubbiesyourbulgywolgyyouresobigoooorubbiesmoreonyourbulgywolgyitdoesntstopgrowingkissesyouandlickiesyourneckydaddylikiesnuzzleswuzzlesIhopedaddyreallylikeswigglesbuttandsquirmsIwanttoseeyourbigdaddymeatwigglesbuttIhavealittleitcho3owagstailcanyoupleasegetmyitchputspawsonyourchestnyeaitsaseveninchitchrubsyourchestcanyouhelpmepweasesquirmspwettypweasesadfaceIneedtobepunishedrunspawsdownyourchestandbitesliplikeIneedtobepunishedreallygoodpawsonyourbulgeasIlickmylipsImgettingthirstyIcangoforsomemilkunbuttonsyourpantsasmyeyesglowyousmellsomuskyvlicksshaftmmmmsomuskydroolsalloveryourcockyourdaddymeatIlikefondlesMrFuzzyBallsheheputssnoutonballsandinhalesdeeplyohgodimsohardlicksballspunishmedaddynyeasquirmsmoreandwigglesbuttIloveyourmuskygoodnessbiteslippleasepunishmelickslipsnyeasucklesonyourtipsogoodlickspreofyourcocksaltygoodnesseyesrolebackandgoesballsdeepmmmmmoansandsuckles`,
	resave: false,
	saveUninitialized: true,
	// cookie: { secure: "auto" },
	store: new MongoStore({
		mongooseConnection: mongoose.connection
	})
};
app.use(session(sessionOptions));

// passport auth middleware
app.use(passport.initialize());
app.use(passport.session());

// create express server
const server = app.listen(port, () => {
	console.log(chalk.green("[Express] Listening on " + port));
});

// create socket io server
const io = socketio(server);

// add our session middleware to socketio
io.use(
	passportsocketio.authorize({
		cookieParser,
		key: sessionOptions.key,
		secret: sessionOptions.secret,
		store: sessionOptions.store,
		success: onAuthSuccess,
		fail: onAuthFail
	})
);

function onAuthSuccess(data, accept) {
	accept(null, true);
}

function onAuthFail(data, message, error, accept) {
	// console.log("fail", message, error);
	accept(null, false);
}

const room = {
	messages: []
};

io.on("connect", socket => {
	if (socket.request.user)
		console.log(
			`${socket.request.user.displayname} on socket ${
				socket.id
			} connected`
		);
	else console.log(`unknown on socket ${socket.id} connected`);

	socket.emit("sync", {
		messages: room.messages.slice(Math.max(0, room.messages.length - 32))
	});

	socket.on("deletemessage", id => {
		room.messages = room.messages.filter(message => message.id !== id);
		socket.emit("sync", {
			messages: room.messages.slice(
				Math.max(0, room.messages.length - 32)
			)
		});
	});

	socket.on("chatmessage", data => {
		const message = {
			id: Date.now(),
			from: socket.request.user
				? pick(socket.request.user, [
						"_id",
						"displayname",
						"profilepic"
				  ])
				: socket.id,
			body: data
		};
		// commands
		if (message.body.startsWith("!")) {
			if (message.body.startsWith("!purge")) {
				const amount = parseInt(
					message.body.slice(message.body.charAt(6) === " " ? 7 : 6)
				);
				if (!isNaN(amount)) {
					room.messages = room.messages.slice(
						0,
						room.messages.length -
							Math.min(Math.max(amount, 0), room.messages.length)
					);
				} else {
					room.messages = [];
				}
				socket.emit("sync", {
					messages: room.messages.slice(
						Math.max(0, room.messages.length - 32)
					)
				});
			}
		} else {
			room.messages.push(message);
			io.emit("chatmessage", message);
		}
	});

	socket.on("disconnect", () => {
		if (socket.request.user)
			console.log(
				`${socket.request.user.displayname} on socket ${
					socket.id
				} disconnected`
			);
		else console.log(`unknown on socket ${socket.id} disconnected`);
	});
});

// register routes
register(app);

// enable ctrl+c to exit
if (process.env.NODE_ENV === "development") {
	readline.emitKeypressEvents(process.stdin);
	process.stdin.setRawMode(true);

	process.stdin.on("keypress", (str, key) => {
		// ctrl + C exit
		if (key.sequence === "\u0003") {
			// close socket io connections
			io.close();
			// kill it
			// eslint-disable-next-line no-process-exit
			process.exit("SIGINT");
		}
	});
}
