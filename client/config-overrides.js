const {
	override,
	addDecoratorsLegacy,
	useEslintRc,
	useBabelRc,
	addWebpackAlias
} = require("customize-cra");
const rewireReactHotLoader = require("react-app-rewire-hot-loader");

const addHotLoader = (config, env) => {
	config = rewireReactHotLoader(config, env);
	return config;
};

module.exports = override(
	addDecoratorsLegacy(),
	addWebpackAlias({
		"react-dom":
			process.env.NODE_ENV === "development"
				? "@hot-loader/react-dom"
				: "react-dom"
	}),
	useBabelRc(),
	useEslintRc(),
	addHotLoader
);
