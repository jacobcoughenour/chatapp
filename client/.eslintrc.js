module.exports = {
	plugins: ["react-app"],
	extends: [
		"plugin:react-app/recommended",
		"plugin:prettier/recommended"
	],
	rules: {
		"prettier/prettier": "warn"
	}
};
