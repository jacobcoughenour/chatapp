import mongoose from "mongoose";

const UserInfoModel = mongoose.model("user", {
	_id: { type: Number, id: true },
	displayname: String,
	profilepic: String,
	lastlogin: { type: Date, default: Date.now }
});

export { UserInfoModel };
