import mongoose from "mongoose";
import chalk from "chalk";

export default class db {
	static init() {
		console.log("[DB] Connecting to MongoDB Atlas...");

		// connect to mongoose server
		mongoose.connect(process.env.MONGO_URL, {
			useNewUrlParser: true,
			useFindAndModify: false
		});

		const db = mongoose.connection;

		db.on("error", () => {
			console.error("[DB] Failed to connect to database");
			console.error("[DB] You might need to whitelist your IP!");
			// eslint-disable-next-line no-process-exit
			process.exit("SIGINT");
		});

		db.once("open", () => {
			console.log(
				chalk.green("[DB] Successfully connected to MongoDB Atlas!")
			);
		});

		return db;
	}
}
