import React, { Component } from "react";
import { hot } from "react-hot-loader";
import { withTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import ChatView from "./containers/ChatView";
import "simplebar/dist/simplebar.min.css";

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isMenuOpen: false,
			isLoading: true,
			profile: {}
		};
	}

	componentDidMount() {
		fetch("/api/users/current")
			.then(res => res.json())
			.then(res => {
				if (res.loggedin)
					this.setState({ isLoading: false, profile: res });
				else this.setState({ isLoading: false });
			})
			.catch(ex => {
				console.error("fetch failed", ex);
			});
	}

	handleMenuToggle = event => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleMenuClose = () => {
		this.setState({ anchorEl: null });
	};

	render() {
		const { theme } = this.props;
		const { isLoading, isMenuOpen, profile, anchorEl } = this.state;

		return (
			<div
				style={{
					position: "absolute",
					top: 0,
					left: 0,
					right: 0,
					bottom: 0,
					width: "100%",
					height: "100%",
					background: theme.palette.background.default,
					display: "flex",
					flexDirection: "column",
					overflow: "hidden"
				}}
			>
				<AppBar position="relative">
					<Toolbar>
						<Typography variant="h6" color="inherit" noWrap>
							jChat
						</Typography>
						<div style={{ flexGrow: 1 }} />
						{isLoading ? (
							""
						) : profile.loggedin ? (
							<React.Fragment>
								<IconButton
									style={{
										margin: 0,
										padding: 4
									}}
									aria-owns={
										isMenuOpen
											? "material-appbar"
											: undefined
									}
									aria-haspopup="true"
									onClick={this.handleMenuToggle}
									color="inherit"
								>
									<img
										src={profile.profilepic}
										alt={profile.displayname}
										style={{
											display: "inline-flex",
											alignItems: "center",
											padding: 0,
											margin: 0,
											borderRadius: "50%",
											height: 32
										}}
									/>
								</IconButton>
								<Menu
									id="profile-menu"
									disableAutoFocusItem
									anchorEl={anchorEl}
									open={Boolean(anchorEl)}
									onClose={this.handleMenuClose}
									MenuListProps={{ style: { paddingTop: 0 } }}
								>
									<MenuItem
										onClick={this.handleMenuClose}
										style={{
											background: theme.palette.grey[900],
											padding: "32px 16px"
										}}
									>
										<ListItemAvatar>
											<Avatar
												style={{
													width: 64,
													height: 64
												}}
												alt={profile.displayname}
												src={profile.profilepic}
											/>
										</ListItemAvatar>
										<ListItemText
											primary={profile.displayname}
											primaryTypographyProps={{
												color: "textPrimary"
											}}
											secondary={""}
											secondaryTypographyProps={{
												color: "textSecondary"
											}}
										/>
									</MenuItem>
									<Divider />
									<MenuItem
										component="button"
										href={
											process.env.NODE_ENV ===
											"development"
												? "http://" +
												  window.location.hostname +
												  ":8080/logout"
												: "/logout"
										}
									>
										<ListItemIcon>
											<ExitToAppIcon />
										</ListItemIcon>
										<ListItemText primary="Sign out" />
									</MenuItem>
								</Menu>
							</React.Fragment>
						) : (
							<Button
								href={
									process.env.NODE_ENV === "development"
										? "http://" +
										  window.location.hostname +
										  ":8080/auth/google"
										: "/auth/google"
								}
							>
								sign in with google
							</Button>
						)}
					</Toolbar>
				</AppBar>
				<div
					style={{
						flex: 1,
						width: "100%"
					}}
				>
					<ChatView profile={profile} />
				</div>
			</div>
		);
	}
}

export default withTheme()(
	process.env.NODE_ENV === "development" ? hot(module)(App) : App
);
