import passport from "passport";
import { OAuth2Strategy } from "passport-google-oauth";
import { UserInfoModel } from "./models";
import pick from "object.pick";

/**
 * Register api routes
 * @param {Express.Application} app
 */
export function register(app) {
	// google login using id_token
	passport.use(
		new OAuth2Strategy(
			{
				clientID: process.env.REACT_APP_OAUTH_CLIENT_ID,
				clientSecret: process.env.GOOGLE_CLIENT_SECRET,
				callbackURL:
					process.env.NODE_ENV === "development"
						? "http://localhost:8080/auth/google/callback"
						: "/auth/google/callback"
			},
			(accessToken, refreshToken, profile, done) => {
				// console.log(accessToken, refreshToken, profile);
				UserInfoModel.findByIdAndUpdate(
					profile.id,
					{
						_id: profile.id,
						displayname: profile.displayName,
						profilepic: profile.photos[0].value,
						lastlogin: Date.now()
					},
					// create new doc in none is found
					{ upsert: true },
					err => {
						if (err) return done(err);

						console.log(
							`[OAuth2] User #${profile.id} login successful!`
						);
						done(null, profile);
					}
				);
			}
		)
	);

	passport.serializeUser((user, done) => {
		done(null, user.id);
	});

	passport.deserializeUser((id, done) => {
		UserInfoModel.findById(id, (err, res) => done(err, res));
	});

	app.get("/api/users/current", (req, res) => {
		if (req.user) {
			res.json({
				loggedin: true,
				...pick(req.user, ["_id", "displayname", "profilepic"])
			});
		} else res.json({ loggedin: false });
	});

	app.get("/logout", (req, res) => {
		req.logout();
		res.redirect(
			process.env.NODE_ENV === "development"
				? "http://localhost:3000/"
				: "/"
		);
	});

	app.get(
		"/auth/google",
		passport.authenticate("google", { scope: ["profile"] })
	);

	app.get(
		"/auth/google/callback",
		passport.authenticate("google", {
			failureRedirect:
				process.env.NODE_ENV === "development"
					? "http://localhost:3000/"
					: "/"
		}),
		(req, res) => {
			res.redirect(
				process.env.NODE_ENV === "development"
					? "http://localhost:3000/"
					: "/"
			);
		}
	);
}
