import { Component, Fragment } from "react";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import { withTheme } from "@material-ui/core/styles";
import SimpleBar from "simplebar-react";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import SendIcon from "@material-ui/icons/Send";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Avatar from "@material-ui/core/Avatar";
import openSocket from "socket.io-client";

@withTheme()
class ChatView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			messages: [],
			input: ""
		};
	}

	_isMounted = false;

	componentDidMount() {
		this._isMounted = true;

		this.socket = openSocket(
			process.env.NODE_ENV === "development"
				? window.location.hostname + ":8080"
				: null
		);

		this.socket.on("connect", () => {
			console.log("connected");

			this.socket.on("sync", data => {
				this.setState({ messages: data.messages });
			});

			this.socket.on("chatmessage", data => {
				this.setState({
					messages: [...this.state.messages, data]
				});
			});

			this.socket.on("disconnect", () => {
				console.log("disconnected");
			});
		});
	}

	componentWillUnmount() {
		// disconnect from server before unmounting
		this.socket.disconnect();
		this._isMounted = false;
	}

	handleInputChange = event => {
		this.setState({ input: event.target.value });
	};

	onSubmit = event => {
		event.preventDefault();
		this.sendChatMessage();
	};

	onDeleteMessage = id => {
		this.socket.emit("deletemessage", id);
	};

	sendChatMessage() {
		if (this.state.input.length === 0) return;

		this.socket.emit("chatmessage", this.state.input);

		this.setState({ input: "" });
	}

	render() {
		const { theme, profile } = this.props;
		const { messages } = this.state;

		return (
			<div
				style={{
					width: "100%",
					height: "100%",
					display: "flex",
					flex: 1,
					flexDirection: "column",
					color: "white"
				}}
			>
				<div
					style={{
						flex: 1,
						position: "relative"
					}}
				>
					<div
						style={{
							position: "absolute",
							overflow: "hidden",
							width: "100%",
							height: "100%"
						}}
					>
						<SimpleBar
							style={{
								position: "relative",
								width: "100%",
								height: "100%",
								overflowX: "hidden"
							}}
						>
							<div
								style={{
									margin: 12
								}}
							>
								{messages.map((message, key) => (
									<div
										key={key}
										className="chat-message"
										css={{
											display: "flex",
											margin: "4px 0",
											"&:hover .message-action": {
												opacity: 1
											}
										}}
										style={{
											justifyContent:
												message.from._id === profile._id
													? "flex-end"
													: "flex-start"
										}}
									>
										{message.from._id !== profile._id ? (
											<Avatar
												css={{
													width: 32,
													height: 32
												}}
												alt={
													message.from.displayname ||
													""
												}
												src={
													message.from.profilepic ||
													""
												}
											/>
										) : (
											<IconButton
												className="message-action"
												aria-label="Delete"
												css={{
													opacity: 0,
													transition: "opacity 0.2s",
													height: 32,
													width: 32,
													margin: 0,
													padding: 4
												}}
												onClick={() =>
													this.onDeleteMessage(
														message.id
													)
												}
											>
												<DeleteIcon color="default" />
											</IconButton>
										)}
										<div
											style={{
												background:
													message.from._id ===
													profile._id
														? theme.palette.primary
																.main
														: theme.palette
																.grey[900],
												color:
													theme.palette.primary
														.contrastText
											}}
											css={{
												borderRadius: 14,
												margin: "0px 8px",
												padding: "6px 12px",
												overflow: "hidden",
												maxWidth: 600,
												"&>*": {
													borderRadius: 8,
													maxHeight: 400
												},
												"&>img, iframe": {
													margin: "0px -6px -4px -6px"
												}
											}}
										>
											{/<[a-z][\s\S]*>/i.test(
												message.body
											) ? (
												<Fragment>
													<div
														dangerouslySetInnerHTML={{
															__html: message.body
														}}
													/>
													<code
														css={{
															opacity: 0.7
														}}
													>
														{message.body}
													</code>
												</Fragment>
											) : (
												<span>{message.body}</span>
											)}
										</div>
									</div>
								))}
							</div>
						</SimpleBar>
					</div>
				</div>
				<form
					style={{
						display: "flex",
						// background: theme.palette.background.paper,
						padding: `${theme.spacing.unit * 2}px ${theme.spacing
							.unit * 2}px ${theme.spacing.unit * 2}px ${theme
							.spacing.unit * 2}px`
					}}
					noValidate
					autoComplete="off"
					onSubmit={this.onSubmit}
				>
					<TextField
						id="outlined-full-width"
						variant="outlined"
						label="Message"
						style={{
							flex: 1,
							margin: 0,
							padding: `0 ${theme.spacing.unit * 2}px 0 0`
						}}
						value={this.state.input}
						onChange={this.handleInputChange}
						margin="normal"
						fullWidth
					/>
					<Fab
						style={{
							margin: 0,
							boxShadow: "none"
						}}
						color="primary"
						onClick={this.onSubmit}
					>
						<SendIcon />
					</Fab>
				</form>
			</div>
		);
	}
}

export default ChatView;
